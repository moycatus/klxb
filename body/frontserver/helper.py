import argparse
from flask import jsonify
from person import Person


def fail(message="Unknown Error"):
    """
    Return a failed message.
    :param message:
    :return:
    """
    return jsonify({"status": "failed", "message": message})


def parse_args():
    """
    Parse arguments.
    :return:
    """
    parser = argparse.ArgumentParser(description="KLXB Body Web Backend.")
    parser.add_argument("--debug", action="store_true",
                        help="Use this option to enable debug mode.")
    parser.add_argument("--host", action='store', default="mysql",
                        help="Specify MySQL host address.")
    parser.add_argument("--port", action='store', default=3306, type=int,
                        help="Specify MySQL host port.")
    return parser.parse_args()


def parse_persons(persons_info):
    """
    Parse persons from response["person_info"][x].
    :param persons_info:
    :return:
    """
    persons = list()
    for person_info in persons_info:
        tags = parse_tags(person_info['attributes'])
        location = person_info['location']
        person = Person(
            location['left'],
            location['top'],
            location['width'],
            location['height'],
            tags
        )
        persons.append(person)
    return persons


def parse_tags(attributes):
    tags = list()
    # Set trust threshold.
    threshold = 0.5
    # Parse gender.
    if attributes['gender']['score'] > threshold:
        tags.append(attributes['gender']['name'])
    # Parse gender.
    if attributes['age']['score'] > threshold:
        tags.append(attributes['age']['name'])
    # Parse upper wear.
    if attributes['upper_wear_fg']['score'] > threshold and attributes['upper_wear_fg']['score'] != '无法确定':
        tags.append(attributes['upper_wear_fg']['name'])
    elif attributes['upper_wear']['score'] > threshold:
        tags.append(attributes['upper_wear']['name'])
    # Parse lower wear.
    if attributes['lower_wear']['score'] > threshold:
        tags.append(attributes['lower_wear']['name'])
    # Parse umbrella.
    if attributes['umbrella']['score'] > threshold and attributes['umbrella']['name'] == '打伞':
        tags.append('打伞')
    # Parse glasses.
    if attributes['glasses']['score'] > threshold and attributes['glasses']['name'] == '戴眼镜':
        tags.append('戴眼镜')
    # Parse cellphone.
    if attributes['cellphone']['score'] > threshold and attributes['cellphone']['name'] == '使用手机':
        tags.append('使用手机')

    # TODO: Add stat for over threshold but not showed.

    return tags
