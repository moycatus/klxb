#!/usr/bin/env python3
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO

class DisplayService(object):
    """
    Compose the person data to photo.
    """
    def __init__(self, image):
        # Save the raw photo.
        self.photo = Image.open(BytesIO(image))
        # Create a transport overlay.
        self.width, self.height = self.photo.size
        self.overlay = Image.new('RGBA', (self.width, self.height))
        self.overlay_draw = ImageDraw.Draw(self.overlay)

    def add_person(self, person):
        """
        Add a person to the photo.
        :param person: a Person object
        :return:
        """
        # Add the square of this person.
        self._add_square(
            person.location["x"],
            person.location["y"],
            person.location["w"],
            person.location["h"]
        )
        # Add the tags of this person.
        self._add_tags(
            person.location["x"],
            person.location["y"],
            person.location["w"],
            person.location["h"],
            person.tags
        )

    def output(self):
        """
        Output photo & overlay.
        :return: photo & overlay BytesIO objects
        """
        output_photo = BytesIO()
        output_overlay = BytesIO()
        # Put the overlay to the photo.
        r, g, b, a = self.overlay.split()
        modified_photo = self.photo.copy()
        modified_photo.paste(self.overlay, mask=a)
        # Output image as BytesIO objects.
        self.overlay.save(output_overlay, 'png')
        modified_photo.save(output_photo, 'png')
        return output_photo, output_overlay

    def _add_tags(
            self, x, y, w, h,
            tags,
            bg_color=(230, 126, 34, 200),
            bg_outline='#E67E22',
            text_color='white',
            text_size=16
    ):
        """
        To add tags.
        :param x: x offset of top-left of square
        :param y: y offset of top-left of square
        :param w: width of square
        :param h: height of square
        :param tags: list of tags
        :param bg_color: background color
        :param bg_outline: background outline
        :param text_color: text color
        :param text_size: text size
        :return: None
        """
        # Load the font.
        font = ImageFont.truetype("font.ttf", text_size)
        # Choose position.
        max_size = text_size * 4
        if x + w + max_size < self.width:
            text_align = 'right'
        else:
            text_align = 'left'
        # Fix positions.
        if text_align == 'right':
            x = x + w
            x += int(text_size * 0.25)
        else:
            y -= int(text_size * 0.25)
        # Draw texts.
        for tag in tags:
            # Get the size of this tag.
            tag_size = self.overlay_draw.textsize(tag, font)
            # Draw the square.
            self.overlay_draw.rectangle(
                (
                    x,
                    y,
                    x + int(tag_size[0] * 1.2),
                    y + int(tag_size[1] * 1.2)
                ),
                fill=bg_color,
                outline=bg_outline
            )
            # Draw the text.
            self.overlay_draw.text(
                (
                    x + int(tag_size[0] * 0.1),
                    y + int(tag_size[1] * 0.1)
                ),
                tag,
                font=font,
                fill=text_color
            )
            # Increase y.
            y += (tag_size[1] * 1.2)

    def _add_square(self, x, y, w, h, color='#E74C3C'):
        """
        To draw a square on the photo.
        :param x: x offset of top-left
        :param y: y offset of top-left
        :param w: width of square
        :param h: height of square
        :return: None
        """
        self.overlay_draw.rectangle(xy=[
            (x, y),
            (x + w, y + h)
        ], outline=color)


if __name__ == '__main__':
    img = open("a.png", "rb")
    bt = img.read()
    ds = DisplayService(bt)
    tags = ["男性", "青年", "戴眼镜"]
    ds._add_tags(10, 10, 200, 200, tags)
    ds._add_square(10, 10, 200, 200)
    photo, overlay = ds.output()
    fp = open("ap.png", "wb")
    fo = open("ao.png", "wb")
    fp.write(photo.getvalue())
    fo.write(overlay.getvalue())
    fp.close()
    fo.close()
