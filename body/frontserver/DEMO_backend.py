from flask import Flask,render_template,jsonify,request
from PIL import Image,ImageDraw
from io import BytesIO
import base64
from random import *
from hashlib import md5
from display import render2img
from baiduapi import body_attr

key = "24.825d601dda7106af9861750d8775bf11.2592000.1529320550.282335-11265041"

app = Flask(__name__, static_url_path='')

@app.route('/', methods=['GET'])
def home():
	return render_template('DEMO_frontend.html')
	
#def render2img(x):
#	'''标记图片 返回base64'''
#	img_data = base64.b64decode(x)
#	img = Image.open(BytesIO(img_data))
#	draw = ImageDraw.Draw(img)
#	xy = [(randint(100,200),randint(100,200)),(randint(500,600),randint(500,600))]
#	draw.rectangle(xy=xy, outline="red")
#	out = BytesIO()
#	img.save(out, 'png')
#	res = str(base64.b64encode(out.getvalue()))[2:-1]
#	print(md5(bytes(res,encoding="latin")).hexdigest())
#	return res
	
@app.route('/photos/', methods=['POST'])
def signin_form():
	img_b64 = request.form["photo_in"]
	bd_data = body_attr(key, img_b64)
#	print(bd_data)
	res = {
		'photo_out': 'data:image/png;base64,'+render2img(img_b64, bd_data),
		'data': bd_data
	}
	return jsonify(res)

if __name__ == '__main__':
	app.run()