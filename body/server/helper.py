import argparse
import time
from flask import jsonify


def timing(message):
    """
    Print a message with current timestamp for debug.
    :param message:
    :return:
    """
    print('[{}] '.format(time.time()), message)


def fail(message="Unknown Error"):
    """
    Return a failed message.
    :param message:
    :return:
    """
    return jsonify({"status": "failed", "message": message})


def parse_args():
    """
    Parse arguments.
    :return:
    """
    parser = argparse.ArgumentParser(description="KLXB Body Web Backend.")
    parser.add_argument("--debug", action="store_true",
                        help="Use this option to enable debug mode.")
    parser.add_argument("--host", action='store', default="0.0.0.0",
                        help="Specify listening host address.")
    parser.add_argument("--port", action='store', default=5000, type=int,
                        help="Specify listening host port.")
    return parser.parse_args()


# Concerned tag categories will be shown in the stat.
CONCERNED_TAG_TYPES = [
    'gender',
    'age',
    'upper_wear',
    'upper_wear_fg',
    'lower_wear',
    'umbrella',
    'glasses',
    'cellphone'
]

# Printed tags will be printed in the photo.
PRINTED_TAGS = [
    '男性',
    '女性',
    '幼儿',
    '青少年',
    '青年',
    '中年',
    '老年',
    '打伞',
    '戴眼镜',
    '使用手机',
    '长裤',
    '短裤',
    '裙',
    '长袖',
    '短袖',
    'T恤',
    '无袖',
    '衬衫',
    '西装',
    '毛衣',
    '夹克',
    '羽绒服',
    '风衣',
    '外套'
]

# Only tags scored over this will be trusted.
TAG_THRESHOLD = 0.5


def parse_tags(attributes):
    """
    Parse tags, return tag stat & to-print tags.
    :param attributes:
    :return:
    """
    tags = {
        'to_print': list(),
        'concerned': dict()
    }

    # Parse tags.
    for attr_type, attr in attributes.items():
        attr_name = attr['name']
        # Filter the non-trusted.
        if attr['score'] < TAG_THRESHOLD:
            continue
        # Filter the non-concerned.
        if attr_type in CONCERNED_TAG_TYPES:
            tags['concerned'][attr_type] = attr_name
        # Filter the non-print.
        if attr_name in PRINTED_TAGS:
            tags['to_print'].append(attr_name)
    return tags
