#!/usr/bin/env python3
import requests
import json
import base64


def get_access_token(ak, sk):
    """
    Get access token.
    :param ak:
    :param sk:
    :return: token string
    """
    data = {
        'grant_type': 'client_credentials',
        'client_id': ak,
        'client_secret': sk
    }
    url = 'https://aip.baidubce.com/oauth/2.0/token'
    req = requests.post(url, data=data)
    return json.loads(req.text)['access_token']


def body_analysis(access_token, image):
    url = 'https://aip.baidubce.com/rest/2.0/image-classify/v1/body_analysis'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'access_token': access_token, 'image': image}
    req = requests.post(url, data=data, headers=headers)
    return json.loads(req.text)


def body_attr(access_token, image):
    url = 'https://aip.baidubce.com/rest/2.0/image-classify/v1/body_attr'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'access_token': access_token, 'image': image}
    req = requests.post(url, data=data, headers=headers)
    return json.loads(req.text)


def body_num(access_token, image, area=None, show=False):
    url = 'https://aip.baidubce.com/rest/2.0/image-classify/v1/body_num'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'access_token': access_token, 'image': image, 'show': str(show).lower()}
    if area is not None:
        data['area'] = area
    req = requests.post(url, data=data, headers=headers)
    return json.loads(req.text)


def main():
    ak = 'NVIil2OkAjocz50Kmhbez08L'
    sk = 'uNX1M0i8eG14bmS4FGaOEtlBWgTbzvQ9'
    access_token = get_access_token(ak, sk)
    print(access_token)
    # fp = open('C:\\Users\\plusl\\Documents\\Tencent Files\\943458526\\FileRecv\\MobileFile\\2.png', 'rb')
    fp = open('C:\\Users\\plusl\\Documents\\Tencent Files\\943458526\\FileRecv\\MobileFile\\IMG_20180404_201004.png',
              'rb')
    image = base64.b64encode(fp.read())
    fp.close()
    # print(body_analysis(access_token, image))
    print(body_attr(access_token, image))
    # print(result)


if __name__ == '__main__':
    main()
