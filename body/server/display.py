#!/usr/bin/env python3
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
from helper import parse_tags

import base64
from baiduapi import body_attr, get_access_token


class DisplayService(object):
    # Stat overall.
    all_stat = dict()

    """
    Compose the person data to photo.
    """
    def __init__(self, image):
        # Save the raw photo.
        self.photo = Image.open(BytesIO(image))
        # Create a transport overlay.
        self.width, self.height = self.photo.size
        self.overlay = Image.new('RGBA', (self.width, self.height))
        self.overlay_draw = ImageDraw.Draw(self.overlay)
        # Create a stat.
        self.stat = dict()

    def add_persons(self, persons_info):
        """
        Parse & add persons from response["person_info"][x].
        :param persons_info:
        :return:
        """
        for person_info in persons_info:
            tags = parse_tags(person_info['attributes'])
            location = person_info['location']
            person = Person(
                location['left'],
                location['top'],
                location['width'],
                location['height'],
                tags
            )
            self.add_person(person)

    def add_person(self, person):
        """
        Add a person to the photo.
        :param person: a Person object
        :return:
        """
        # Add the square of this person.
        self._add_square(
            person.location["x"],
            person.location["y"],
            person.location["w"],
            person.location["h"]
        )
        # Add the tags of this person.
        self._add_tags(
            person.location["x"],
            person.location["y"],
            person.location["w"],
            person.location["h"],
            person.tags['to_print']
        )
        # Add the concerned tags to the stat.
        self._add_stat(person.tags['concerned'])

    def output(self, with_photo=False):
        """
        Output photo & overlay.
        :return: photo & overlay BytesIO objects
        """
        output_photo = BytesIO()
        output_overlay = BytesIO()
        # Put the overlay to the photo if required.
        if with_photo:
            r, g, b, a = self.overlay.split()
            modified_photo = self.photo.copy()
            modified_photo.paste(self.overlay, mask=a)
            modified_photo.save(output_photo, 'png')
        # Output overlay as BytesIO objects.
        self.overlay.save(output_overlay, 'png')
        return output_photo, output_overlay

    def _add_tags(
            self, x, y, w, h,
            tags,
            bg_color=(231, 76, 60, 200),
            bg_outline='#E74C3C',
            text_color='white',
            text_size=16
    ):
        """
        To add tags.
        :param x: x offset of top-left of square
        :param y: y offset of top-left of square
        :param w: width of square
        :param h: height of square
        :param tags: list of tags
        :param bg_color: background color
        :param bg_outline: background outline
        :param text_color: text color
        :param text_size: text size
        :return: None
        """
        # Load the font.
        font = ImageFont.truetype("font.ttf", text_size)
        # Choose position.
        max_size = text_size * 4
        if x + w + max_size < self.width:
            text_align = 'right'
        else:
            text_align = 'left'
        # Fix positions.
        if text_align == 'right':
            x = x + w
            x += int(text_size * 0.25)
        else:
            y -= int(text_size * 0.25)
        # Draw texts.
        for tag in tags:
            # Get the size of this tag.
            tag_size = self.overlay_draw.textsize(tag, font)
            # Draw the square.
            self.overlay_draw.rectangle(
                (
                    x,
                    y,
                    x + int(tag_size[0] + text_size * 0.4),
                    y + int(tag_size[1] + text_size * 0.4)
                ),
                fill=bg_color,
                outline=bg_outline
            )
            # Draw the text.
            self.overlay_draw.text(
                (
                    x + int(text_size * 0.2),
                    y + int(text_size * 0.2)
                ),
                tag,
                font=font,
                fill=text_color
            )
            # Increase y.
            y += (tag_size[1] + text_size * 0.5)

    def _add_square(self, x, y, w, h, color='#E74C3C'):
        """
        To draw a square on the photo.
        :param x: x offset of top-left
        :param y: y offset of top-left
        :param w: width of square
        :param h: height of square
        :return: None
        """
        self.overlay_draw.rectangle(xy=[
            (x, y),
            (x + w, y + h)
        ], outline=color)

    def _add_stat(self, concerned_tags):
        """
        Add concerned tags to stat data.
        :param concerned_tags:
        :return:
        """
        for concerned_tag_type, concerned_tag in concerned_tags.items():
            # Add key if not existing.
            if concerned_tag_type not in self.stat:
                self.stat[concerned_tag_type] = dict()
            if concerned_tag_type not in self.all_stat:
                self.all_stat[concerned_tag_type] = dict()
            # Add tag if not existing.
            if concerned_tag not in self.stat[concerned_tag_type]:
                self.stat[concerned_tag_type][concerned_tag] = 0
            if concerned_tag not in self.all_stat[concerned_tag_type]:
                self.all_stat[concerned_tag_type][concerned_tag] = 0
            # Add count.
            self.stat[concerned_tag_type][concerned_tag] += 1
            self.all_stat[concerned_tag_type][concerned_tag] += 1


class Person(object):
    def __init__(self, x, y, w, h, tags):
        self.location = {
            "x": x,
            "y": y,
            "w": w,
            "h": h
        }
        self.tags = tags


if __name__ == '__main__':
    API_KEY = 'dWGfg15eSFHvM8O4lg4b2BfB'
    API_SECRET = '21dBLm0rVFuygKv8vIdl1LuMPHLOy8v2'
    API_TOKEN = get_access_token(API_KEY, API_SECRET)

    img = open("c.png", "rb")
    bt = img.read()

    # Post to Baidu API & parse the response.
    parsed_info = body_attr(API_TOKEN, base64.b64encode(bt))
    # Get raw photo bytes & compose into photos.
    photo_composer = DisplayService(bt)
    try:
        photo_composer.add_persons(parsed_info["person_info"])
    except Exception:
        print(parsed_info)
    # Output
    photo, overlay = photo_composer.output(with_photo=True)
    fp = open("cp.png", "wb")
    #fo = open("ao.png", "wb")
    fp.write(photo.getvalue())
    #fo.write(overlay.getvalue())
    fp.close()
    #fo.close()
