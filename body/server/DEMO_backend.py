from flask import Flask, jsonify, request
import base64
from random import randint
from baiduapi import body_attr
from baiduapi import get_access_token
from display import DisplayService
from helper import parse_args
from helper import fail

API_KEY = 'dWGfg15eSFHvM8O4lg4b2BfB'
API_SECRET = '21dBLm0rVFuygKv8vIdl1LuMPHLOy8v2'
API_TOKEN = get_access_token(API_KEY, API_SECRET)

app = Flask(__name__, static_url_path='/static')


@app.route('/', methods=['GET'])
def home():
    return app.send_static_file('DEMO_frontend.html')


@app.route('/photos/', methods=['POST'])
def process():
    # Read from request.
    try:
        photo_b64 = request.form["photo_in"]
    except KeyError:
        return fail("No Photo")
    # Post to Baidu API & parse the response.
    try:
        parsed_info = body_attr(API_TOKEN, photo_b64)
    except Exception as e:
        return fail("API Error: " + str(e))
    # Get raw photo bytes & compose into photos.
    try:
        photo_bytes = base64.b64decode(photo_b64)
        photo_composer = DisplayService(photo_bytes)
        photo_composer.add_persons(parsed_info["person_info"])
        photo, overlay = photo_composer.output(with_photo=True)
    except Exception as e:
        raise e
        return fail("Composing Error: " + str(e))
    # Output the result.
    res = {
        'photo_out': 'data:image/png;base64,' + base64.b64encode(overlay.getvalue()).decode(),
        'data': randint(10000, 99999),
        'stat': {
            'current': photo_composer.stat,
            'all': photo_composer.all_stat
        }
    }
    return jsonify(res)


if __name__ == '__main__':
    args = parse_args()
    stat = dict()
    app.run(debug=args.debug, host=args.host, port=args.port)
